#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
}


double Customer::totalSum() const
{
	double sum = 0;
	for (auto itr = _items.begin(); itr != _items.end(); ++itr)
	{
		sum += itr->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	set<Item>::iterator itr;
	itr = _items.find(item);
	if (itr != this->_items.end())
	{
		item.setCount(item.getCount() + 1);
	}
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	this->_items.erase(item);
}

string Customer::getName()
{
	return this->_name;
}

set<Item> Customer::getItems()
{
	return this->_items;
}

void Customer::setName(string name)
{
	this->_name = name;
}

void Customer::setItems(set<Item> items)
{
	this->_items = items;
}
