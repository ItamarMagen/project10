#pragma once
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

class Item
{
public:
	Item(string name, string num , double price);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator<(const Item& other) const; //compares the _serialNumber of those items.
	bool operator>(const Item& other) const; //compares the _serialNumber of those items.
	bool operator==(const Item& other) const; //compares the _serialNumber of those items.

	//getters 
	string getName();
	string getSerialNumber();
	int getCount();
	double getPrice();

	//setters
	void setName(string name);
	void setSerialNumber(string num);
	void setCount(int count);
	void setPrice(double price);
											   

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};


