#include "Item.h"

Item::Item(string name, string num , double price)
{
	this->_name = name;
	this->_serialNumber = num;
	this->_count = 1;
	this->_unitPrice = price;
}

Item::~Item()
{

}

//getters
string Item::getName()
{
	return this->_name;
}

string Item::getSerialNumber()
{
	return this->_serialNumber;
}

int Item::getCount()
{
	return this->_count;
}

double Item::getPrice()
{
	return this->_unitPrice;
}

//setters
void Item::setName(string name)
{
	this->_name = name;
}

void Item::setSerialNumber(string num)
{
	this->_serialNumber = num;
}

void Item::setCount(int count)
{
	this->_count = count;
}

void Item::setPrice(double price)
{
	this->_unitPrice = price;
}

double Item::totalPrice()const
{
	return this->_count * this->_unitPrice;
}

//operators
bool Item::operator<(const Item & other)const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return this->_serialNumber == other._serialNumber;
}
