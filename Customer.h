#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	//getters
	string getName();
	set<Item> getItems();

	//setters
	void setName(string name);
	void setItems(set<Item> items);

private:
	string _name;
	set<Item> _items;
};